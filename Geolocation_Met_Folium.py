#!/usr/bin/env python
# Données téléchargées depuis cette page :
# https://data.beta.grandlyon.com/fr/datasets/1b31ab07-ebaa-4232-8c32-f80a5595190f/info

# Carte qui recense les points de distribution du journal de la Métropole Lyonnaise
# Le Met'

import csv

import folium


# CONSTANTES

NOM_FICHIER_CSV  = 'point_distribution_met.csv'
NOM_FICHIER_HTML = 'Carte_Points_Distribution_Met.html'
ZOOM_CARTE       = 13

TUILES      = 'OpenStreetMap'
ATTRIBUTION = '© Contributeurs OpenStreetMap'


# FONCTIONS

def csv_vers_liste(nom_fichier):
    '''Fonction principale qui prend comme argument le nom du fichier
    et le traite pour créer une liste de liste'''
    with open(nom_fichier, 'r') as fichier_csv:
# On ignore l'entête.
        fichier_csv.readline()

        lecture_fichier = csv.reader(fichier_csv, delimiter=';')

        liste_totale = []

        for lignes in lecture_fichier:
# on ignore les éventuelles lignes vides du fichier
            if lignes[11] != '0':
                liste_totale.append(lignes)

    return liste_totale


def coord_centre(liste):
    '''Fonction qui renvoie la moyenne de la latitude et longitude
    de toutes les photos (lignes du fichier), cela permet de centrer
    la carte'''
    somme_lat = 0
    somme_lon = 0

    moyenne_lat = 0
    moyenne_lon = 0

    for lignes in liste:
        longitude  = float(lignes[10])
        latitute = float(lignes[11])

        somme_lat += latitute
        somme_lon += longitude

    nombre_ligne = len(liste)

    moyenne_lat = somme_lat / nombre_ligne
    moyenne_lon = somme_lon / nombre_ligne

    print("Latitude moyenne = {}, Longitude moyenne = {}".format(moyenne_lat,moyenne_lon))

    return (moyenne_lat, moyenne_lon)


# CORPS DU CODE
liste_fichier = csv_vers_liste(NOM_FICHIER_CSV)

moyenne_lat , moyenne_lon = coord_centre(liste_fichier)

carte = folium.Map(
    location   = [moyenne_lat, moyenne_lon],
    zoom_start = ZOOM_CARTE,
    tiles      = TUILES,
    attr       = ATTRIBUTION
)

#Boucle de création des marqueurs
for donnees in liste_fichier:
    aide        = 'Plus d\'info !'
    nom       = donnees[0]
    adresse = donnees[1]
    adresse_complement =  donnees[2]
    cp = donnees[3]
    ville = donnees[4]
    type_1 = donnees[5]
    sous_type = donnees[6]
    
    longitude_marqueur  = float(donnees[10])
    latitude_marqueur = float(donnees[11])

    folium.Marker(
        [latitude_marqueur, longitude_marqueur],
        popup="<h3>{}</h3> <b>Type</b> : {}, <br> <i>Sous-type : {}</i> <br><br> <b>Adresse</b> :<br> <i>{}</i> <br> <i>{}</i> <br> <i>{}</i> <i>{}</i> <br>".format(nom,type_1, sous_type, adresse,adresse_complement,cp,ville),
        tooltip = aide,
        icon    = folium.Icon(color='green', icon='newspaper-o', prefix='fa')
    ).add_to(carte)

#On sauve la carte au format HTML
carte.save(NOM_FICHIER_HTML)
print("La carte HTML, nommée {} a été générée dans le dossier courant".format(NOM_FICHIER_HTML))
